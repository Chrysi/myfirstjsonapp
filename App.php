<?php

$strNumber = $_POST["number"];
$number = intval($strNumber);
$numbers = [
    "double" => $number * 2,
    "minusOne" => $number -1,
    "plusOne" => $number + 1,
];
header("Content-Type: application/json");
echo json_encode($numbers);
